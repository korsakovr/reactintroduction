import logo from '../images/logo.svg';
import './App.css';
import Login from './Login.jsx';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Login/>
      </header>
    </div>
  );
}

export default App;
