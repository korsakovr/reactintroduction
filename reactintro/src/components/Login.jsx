import React, { useState } from 'react';
import './Login.css';
import axios from 'axios';


export default function Login() {
    const [email, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        axios.post(`api`, { email, password })
            .then(
                res => {
                    console.log(res);
                    console.log(res.data);
                    alert("Successfully!");
                },
                error => {
                    alert(error);
                }
            );
    }
    return (
        <div className="login-wrapper">
            <h1>Please Log In</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <p>Username</p>
                    <input type="text" onChange={e => setUserName(e.target.value)} />
                </label>
                <label>
                    <p>Password</p>
                    <input type="password" onChange={e => setPassword(e.target.value)} />
                </label>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}