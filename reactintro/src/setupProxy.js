const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
    console.log("proxy");
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://reqres.in/api/login',
            secure: false,
            changeOrigin: true
        })
    );
};